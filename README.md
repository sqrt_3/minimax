# Minimax

### Note: This project was made with the latest Unity Version (2017.3.1f1)

Tic Tac Toe AI assignment, "unbeatable" AI will cause smart players to Draw at best, since it takes advantage of a Minimax algorithm to decide and evaluate moves thinking as much as 7 moves ahead and evaluating all possible outcomes, then selecting the one that is most favorable for itself.