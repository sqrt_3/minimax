﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class Space : MonoBehaviour
{
    [SerializeField]
    private int row;
    [SerializeField]
    private int column;

    private Text textComp;
    private GameManager.GridSpace node;

    private GameManager gameManager;
    

    private void Awake()
    {
        textComp = GetComponent<Text>();
    }

    private void Start()
    {
        GameObject gameManagerGO = GameObject.FindGameObjectWithTag(GameManager.Tag);
        if (gameManagerGO)
        {
            gameManager = gameManagerGO.GetComponent<GameManager>();
        }
        else
        {
            Debug.LogErrorFormat("A GameManager with the tag {0} could not be found", GameManager.Tag);
        }
    }

    private void Update()
    {
        node = gameManager.GetGridSpace(row, column);
        if (node != null)
        {
            textComp.text = node.player == 1 ? gameManager.Player1 : gameManager.Player2;
        }
    }

    public void PointerClick()
    {
        gameManager.SelectSpace(row, column);
    }
}
