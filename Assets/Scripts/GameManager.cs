﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public class GridSpace
    {
        public int row;
        public int column;
        public int player;

        public GridSpace(int row, int column)
        {
            this.row = row;
            this.column = column;
            this.player = 0;
    }

        public GridSpace(int row, int column, int player)
        {
            this.row = row;
            this.column = column;
            this.player = player;
        }

        public override string ToString()
        {
            return "(" + row + ", " + column + ")";
        }
    }

    public class Node
    {
        public GridSpace move;
        public int score;

        public Node(GridSpace move, int score)
        {
            this.move = move;
            this.score = score;
        }

        public override string ToString()
        {
            return "Gridspace: " + move + " of score: " + score;
        }
    }

    public const string Tag = "GameController"; 

    public string Player1 = "X";
    public string Player2 = "O";

    [SerializeField]
    private int startingPlayer = 1;
    [SerializeField]
    private GameObject gameOverPanel;
    private Text gameOverText;

    private GridSpace[,] grid;
    private List<Node> potentialNodeList = new List<Node>();
    private int currentPlayer;

    private enum Player
    {
        HUMAN = 1,
        AI,
        MAX_PLAYER
    };

    private void Awake()
    {
        if (gameOverPanel == null)
        {
            Debug.LogError("Game Over Panel is not assigned in the Inspector");
        }
        else
        {
            gameOverText = gameOverPanel.GetComponentInChildren<Text>();
            if (gameOverText == null)
            {
                Debug.LogError("There is no Text component on a child of the Game Over Panel");
            }
        }

        grid = new GridSpace[3, 3];
        currentPlayer = startingPlayer;
    }

    private void Start()
    {
        if (currentPlayer == (int)Player.AI)
        {
            AIMove();
        }
    }

    /// <summary>
    /// Returns the Node at given row and column
    /// </summary>
    /// <param name="row">Row of Node to return</param>
    /// <param name="column">Column of Node to return</param>
    /// <returns></returns>
    public GridSpace GetGridSpace(int row, int column)
    {
        return grid[row, column];
    }

    /// <summary>
    /// Fill the grid space at the given row and column if it is a valid move
    /// </summary>
    /// <param name="row">Row of grid space to fill</param>
    /// <param name="column">Column of grid space to fill</param>
    public void SelectSpace(int row, int column)
    {
        if(CheckValidMove(row,column))
        {
            GridSpace node = new GridSpace(row, column, currentPlayer);
            grid[row, column] = node;

            //problem - if game winning move was the last move, then a draw is assumed
            //check winner every move including last move

            int winner = CheckWinner();
            if (winner != 0)
            {
                gameOverPanel.SetActive(true);
                gameOverText.text = string.Format("Player {0} Wins!", currentPlayer);
            }
            else
            {
                //check for remaining moves after checking for winner; do AI moves only when there's moves left
                if(GetAvailableMoves().Count > 0)
                {
                    currentPlayer = currentPlayer == 1 ? 2 : 1;
                    if (currentPlayer == (int)Player.AI)
                    {
                        AIMove();
                    }
                }
                //only at this point can a draw be truly evaluated
                else
                {
                    gameOverPanel.SetActive(true);
                    gameOverText.text = "Draw";
                }
            }
            
        }
    }

    /// <summary>
    /// Checks if a grid space has been filled
    /// </summary>
    /// <param name="row">Row of grid space to check</param>
    /// <param name="column">Column of grid space to check</param>
    /// <returns>True if the space is not filled</returns>
    private bool CheckValidMove(int row, int column)
    {
        return GetGridSpace(row, column) == null;
    }

    /// <summary>
    /// Returns the winning player
    /// </summary>
    /// <returns>1 if Player 1, 2 if Player 2, 0 if no winner yet</returns>
    private int CheckWinner()
    {
        for(int i = 0; i < 3; i++)
        {
            // Rows
            if(grid[i, 0] != null && grid[i, 1] != null && grid[i, 2] != null &&
               grid[i, 0].player == grid[i, 1].player &&
               grid[i, 0].player == grid[i, 2].player)
            {
                return grid[i, 0].player;
            }

            // Columns
            if (grid[0, i] != null && grid[1, i] != null && grid[2, i] != null &&
              grid[0, i].player == grid[1, i].player &&
              grid[0, i].player == grid[2, i].player)
            {
                return grid[0, i].player;
            }
        }

        // Diagonals
        if (grid[0, 0] != null && grid[1, 1] != null && grid[2, 2] != null &&
              grid[0, 0].player == grid[1, 1].player &&
              grid[0, 0].player == grid[2, 2].player)
        {
            return grid[0, 0].player;
        }
        if (grid[2, 0] != null && grid[1, 1] != null && grid[0, 2] != null &&
              grid[2, 0].player == grid[1, 1].player &&
              grid[2, 0].player == grid[0, 2].player)
        {
            return grid[2, 0].player;
        }

        return 0;
    }

    /// <summary>
    /// Generate the possible moves and select the best one for the AI to do
    /// </summary>
    private void AIMove()
    {
        
        // Reset the node list
        potentialNodeList.Clear();

        // Call Minimax on the AI's turn
        Minimax(0, (int)Player.AI);

        // Determine the best move from the potential moves - sorted thru LINQ: best move will be first on this new list
        var bestValue = from n in potentialNodeList
                          orderby n.score descending
                          select n;
        
        
        // Select the best move
        SelectSpace(bestValue.ElementAt(0).move.row, bestValue.ElementAt(0).move.column);

        currentPlayer = (int)Player.HUMAN;
        
    }

    /// <summary>
    /// Minimax Algorithm 
    /// </summary>
    /// <param name="depth"></param>
    /// <param name="turn"></param>
    private int Minimax(int depth, int turn)
    {
        
        // Check if there is a winner on the current board
        // If the winner is the player assign a negative value
        // If the winner is the AI assign a positive value
        if (CheckWinner() == (int)Player.HUMAN)
        {
            return -10;
        }

        else if(CheckWinner() == (int)Player.AI)
        {
            return 10;
        }


        // Get the available moves check to see how many remain
        // Return 0 if there are no more moves (game ended in a draw)
        List<GridSpace> moves = GetAvailableMoves();
        if (moves.Count == 0)
            return 0;


        // Create a list to store the scores
        List<int> scores = new List<int>();

        // For each available move
        // Play a move at that position on the grid
        // Call Minimax for the other player and store the returned score
        // If the we are a top level node, add it to the potential node list
        // Remove the move from the grid
        for(int i=0; i < moves.Count; ++i)
        {
            PlayMove(moves[i], turn);
            int score = Minimax(depth + 1, Opposite(turn));
            scores.Add(score);
            if(depth == 0)
            {
                Node n = new Node(moves[i], score);
                potentialNodeList.Add(n);
            }
            UndoMove(moves[i]);
        }

        // Return the maximum score if it is the AI's turn
        // Return the minimum score if it is the player's turn
        scores.Sort();
        return (turn == (int)Player.AI) ? scores[scores.Count - 1] : scores[0];
    }

    /// <summary>
    /// Returns a list of availables moves
    /// </summary>
    /// <returns>List of Nodes representing the available moves</returns>
    private List<GridSpace> GetAvailableMoves()
    {
        List<GridSpace> result = new List<GridSpace>();

        for (int row = 0; row < 3; row++)
        {
            for (int column = 0; column < 3; column++)
            {
                if (CheckValidMove(row, column))
                {
                    GridSpace newNode = new GridSpace(row, column);
                    result.Add(newNode);
                }
            }
        }

        return result;
    }

    public void PlayMove(GridSpace space, int player)
    {
        grid[space.row, space.column] = space;
        grid[space.row, space.column].player = player;
    }

    public void UndoMove(GridSpace space)
    {
        grid[space.row, space.column] = null;
    }

    public int Opposite(int player)
    {
        return (player == (int)Player.AI) ? (int)Player.HUMAN : (int)Player.AI;
    }
}
